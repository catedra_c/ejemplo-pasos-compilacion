# Ejemplo de pasos de compilación de un programa en C

1. Compilar todo en un solo paso:
     `gcc -Wall -o imprimir main.c imprimir.c`
2. Sólo preprocesar:
     `gcc -Wall -E main.c imprimir.c`
3. Preprocesar y compilar a assembler:
     `gcc -Wall -S main.c imprimir.c`
4. Preprocesar, compilar y ensamblar a código objeto:
     `gcc -Wall -c main.c imprimir.c`
