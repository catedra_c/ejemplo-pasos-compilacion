#include <stdio.h>
#include <math.h>
#define  TAMANO_BUFFER        2
#define  TAMANO_TERMINADOR    1
#define  MAXIMO_VALOR_POSIBLE (int)(pow(10, TAMANO_BUFFER - TAMANO_TERMINADOR) - 1)
#define  EXITO_IMPRIMIR       0
#define  ERROR_IMPRIMIR       1

int imprimir(unsigned int);

