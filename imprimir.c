#include "imprimir.h"

int imprimir(unsigned int numero) {
  printf("Imprimiendo: ");
  if (numero > MAXIMO_VALOR_POSIBLE) {
    return ERROR_IMPRIMIR;
  }

  char buffer[TAMANO_BUFFER];
  sprintf(buffer, "%d", numero);
  printf("%s\n", buffer);

  return EXITO_IMPRIMIR;
}

