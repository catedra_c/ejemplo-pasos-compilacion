#include <stdio.h>
#include "imprimir.h"
#define  REPETICIONES MAXIMO_VALOR_POSIBLE * 2

int main(int argc, char *argv[]) {
  int retorno = EXITO_IMPRIMIR;
  printf("Repeticiones: %d\n", REPETICIONES);

  for (unsigned int i = 1; i <= REPETICIONES && retorno == 0; i++) {
    retorno = retorno | imprimir(i);
  }

  return retorno;
}

